## Installation

1) //-- add in the composer.json of your project --//

    "require": {
        // ...
        "gkratz/editorbundle": "dev-master"
    }


2) //-- activate the bundle in your app/AppKernel.php --//

    new Gkratz\EditorBundle\GkratzEditorBundle(),


3) //-- enter this command in your terminal --//

    php composer update gkratz/editorbundle


## How to use

1) //-- in the builder of your form, use the gkeditor formtype by this way --//

    $builder->add('search', \Gkratz\EditorBundle\Form\Type\GkeditorType::class, array(
        'attr' => array(
            'class' => 'gkeditor' //necessary
        )
    ));


2) //-- import the js into your twig file --//

    <script src="{{ asset('bundles/gkratzeditor/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('bundles/gkratzeditor/tinymce/utinymce.js') }}"></script>
